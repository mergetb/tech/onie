#!/bin/bash

./clone-onie
cd src/onie/build-config
make -j`nproc` MACHINE=kvm_x86_64 all
cp ../build/images/* ~/build/
